package com.alex.testapp923.repository

import android.annotation.SuppressLint
import android.location.Location
import com.google.android.gms.location.FusedLocationProviderClient

class Repository(private val fusedLocationProviderClient: FusedLocationProviderClient) :
    LocationRepository {

    @SuppressLint("MissingPermission")
    override fun getLastKnowLocation(func: (location: Location?) -> Unit) {
        fusedLocationProviderClient.lastLocation.addOnCompleteListener {
            func.invoke(it.result)
        }
    }
}