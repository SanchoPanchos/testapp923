package com.alex.testapp923.repository

import android.location.Location

interface LocationRepository {

    fun getLastKnowLocation(func: (location: Location?) -> Unit)
}