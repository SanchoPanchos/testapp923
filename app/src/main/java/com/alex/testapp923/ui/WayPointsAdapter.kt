package com.alex.testapp923.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.alex.testapp923.R
import kotlinx.android.synthetic.main.item_waypoint.view.*

class WayPointsAdapter(private val actionListener: ActionListener) :
    RecyclerView.Adapter<WayPointsAdapter.ViewHolder>() {

    private val wayPointNames = mutableListOf<String>()

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {
            itemView.ivDeleteWaypoint.setOnClickListener {
                actionListener.invoke(adapterPosition)
            }
        }

        fun bind(wayPointName: String) {
            itemView.tvStopCount.text = itemView.context.getString(
                R.string.stop_count,
                adapterPosition + 1
            )
            itemView.tvStopName.text = wayPointName
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_waypoint, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount() = wayPointNames.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(wayPointNames[position])
    }

    fun submit(newWayPointNames: List<String>) {
        val diffUtilResult =
            DiffUtil.calculateDiff(WayPointsDiffUtilCallback(wayPointNames, newWayPointNames))
        wayPointNames.clear()
        wayPointNames.addAll(newWayPointNames)
        diffUtilResult.dispatchUpdatesTo(this)
    }
}

typealias ActionListener = (wayPointPosition: Int) -> Unit