package com.alex.testapp923.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.alex.testapp923.repository.LocationRepository
import com.here.sdk.core.GeoCoordinates
import com.here.sdk.core.GeoPolyline
import com.here.sdk.core.LanguageCode
import com.here.sdk.routing.CarOptions
import com.here.sdk.routing.RoutingEngine
import com.here.sdk.routing.Waypoint
import com.here.sdk.search.SearchEngine
import com.here.sdk.search.SearchOptions
import com.here.sdk.search.SuggestCallback
import com.here.sdk.search.TextQuery
import java.util.*
import kotlin.concurrent.timer


class RouteViewModel(private val repository: LocationRepository) : ViewModel() {

    private val defaultKmSpeed = 50 * 1.60
    private val defaultLocation: GeoCoordinates = GeoCoordinates(28.1516267, 10.674994)

    private lateinit var timer: Timer
    private var locationUpdateFrequency: Long = 0
    private var currentGeoPointCount: Int = 0

    private val _currentLocationLiveData = MutableLiveData<GeoCoordinates>()
    val currentLocationLiveData: LiveData<GeoCoordinates> = _currentLocationLiveData
    private var currentLocation: GeoCoordinates? = null

    private val _suggestionsLiveData = MutableLiveData<List<Pair<String, GeoCoordinates>>>()
    val suggestionsLiveData: LiveData<List<Pair<String, GeoCoordinates>>> = _suggestionsLiveData

    private val _routeGeoCoordinatesLiveData = MutableLiveData<List<Pair<String, GeoCoordinates>>>()
    val routeGeoCoordinatesLiveData: LiveData<List<Pair<String, GeoCoordinates>>> =
        _routeGeoCoordinatesLiveData
    private val routeGeoCoordinates = mutableListOf<GeoCoordinates>()

    private val _routePolylineLiveData = MutableLiveData<GeoPolyline?>()
    val routePolylineLiveData: LiveData<GeoPolyline?> = _routePolylineLiveData

    private val routePoints = mutableListOf<Pair<String, GeoCoordinates>>()

    private val searchEngine = SearchEngine()
    private val maxSuggestions = 3
    private val searchOptions = SearchOptions(LanguageCode.EN_US, maxSuggestions)
    private val suggestCallback: SuggestCallback = SuggestCallback { error, suggestionList ->
        if (error != null || suggestionList == null) {
            return@SuggestCallback
        }

        _suggestionsLiveData.value = suggestionList.filter {
            it.place != null && it.place?.geoCoordinates != null
        }.map {
            Pair(it.title, it.place?.geoCoordinates!!)
        }
    }


    fun loadCurrentLocation() {
        repository.getLastKnowLocation {
            currentLocation = if (it == null) {
                null
            } else {
                GeoCoordinates(it.latitude, it.longitude)
            }
            currentLocation?.let { location ->
                _currentLocationLiveData.value = location
            }
        }
    }

    fun addStop(title: String, location: GeoCoordinates) {
        routePoints.add(Pair(title, location))
        _routeGeoCoordinatesLiveData.value = routePoints
    }

    fun removeStop(stopPosition: Int) {
        routePoints.removeAt(stopPosition)
        _routeGeoCoordinatesLiveData.value = routePoints
    }

    fun startSimulation() {
        timer = timer(period = locationUpdateFrequency, action = {
            if (currentGeoPointCount == routeGeoCoordinates.size) {
                cancel()
                return@timer
            }
            _currentLocationLiveData.postValue(routeGeoCoordinates[currentGeoPointCount])
            currentGeoPointCount++
        })
    }

    fun buildRoute() {
        val wayPoints = mutableListOf<Waypoint>().apply {
            currentLocation?.let {
                add(Waypoint(it))
            }
            addAll(routePoints.map { Waypoint(it.second) })
        }

        RoutingEngine().calculateRoute(
            wayPoints,
            CarOptions()
        ) { routingError, routes ->
            if (routingError != null || routes == null) {
                return@calculateRoute
            }

            val averageDistanceBetweenGeoPoints = routes[0].lengthInMeters / routes[0].polyline.size
            locationUpdateFrequency =
                ((defaultKmSpeed * 1000 * averageDistanceBetweenGeoPoints) / 3600).toLong()
            routeGeoCoordinates.clear()
            routeGeoCoordinates.addAll(routes[0].polyline)

            _routePolylineLiveData.value = GeoPolyline(routes[0].polyline)
        }
    }

    fun pauseSimulation() {
        timer.cancel()
    }

    fun endSimulation() {
        timer.cancel()
        routeGeoCoordinates.clear()
        _routePolylineLiveData.value = null
    }

    fun searchPlaces(query: String) {
        searchEngine.suggest(
            TextQuery(query, currentLocation ?: defaultLocation),
            searchOptions,
            suggestCallback
        )
    }

}