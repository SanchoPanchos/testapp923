package com.alex.testapp923.ui

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.alex.testapp923.R
import com.alex.testapp923.utils.DisplayUtils
import com.alex.testapp923.utils.hideKeyboard
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import com.here.sdk.core.GeoCoordinates
import com.here.sdk.core.GeoPolyline
import com.here.sdk.core.Metadata
import com.here.sdk.core.Point2D
import com.here.sdk.gestures.TapListener
import com.here.sdk.mapviewlite.*
import com.here.sdk.mapviewlite.PickMapItemsCallback
import kotlinx.android.synthetic.main.bottom_sheet_build_route.*
import kotlinx.android.synthetic.main.fragment_map.*
import org.koin.android.ext.android.inject


class MapFragment : Fragment(R.layout.fragment_map) {

    private val keyMarkerTitle = "markerTitle"
    private val keyMarkerLatitude = "markerLatitude"
    private val keyMarkerLongitude = "markerLongitude"

    private val viewModel: RouteViewModel by inject()
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>
    private lateinit var wayPointsAdapter: WayPointsAdapter
    private val permissionCode = 1

    private lateinit var mapCircleStyle: MapCircleStyle
    private lateinit var mapPolylineStyle: MapPolylineStyle
    private lateinit var mapMarkerImageStyle: MapMarkerImageStyle

    private lateinit var suggestionsAdapter: ArrayAdapter<String>

    private var currentLocationMarker: MapMarker? = null
    private var routePolyline: MapPolyline? = null
    private val routeStops = mutableListOf<MapMarker>()
    private var firstLocationUpdate = true

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (permissionCode == requestCode) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                viewModel.loadCurrentLocation()
                return
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initMap(savedInstanceState)
        initBottomSheetBehavior()
        initAdapters()
        initBottomSheetListeners()
        initListeners()
        initViewModelListeners()
        if (!permissionGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1)
            return
        }

        viewModel.loadCurrentLocation()
    }

    private fun initAdapters() {
        wayPointsAdapter = WayPointsAdapter {
            viewModel.removeStop(it)
        }
        recycler.layoutManager = LinearLayoutManager(requireContext())
        recycler.adapter = wayPointsAdapter

        suggestionsAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_dropdown_item_1line,
            mutableListOf()
        )
        etStopName.setAdapter(suggestionsAdapter)
    }

    private fun initViewModelListeners() {
        viewModel.currentLocationLiveData.observe(viewLifecycleOwner, Observer {
            onLocationChanged(it)
        })
        viewModel.suggestionsLiveData.observe(viewLifecycleOwner, Observer {
            onReceivedSuggestions(it)
        })
        viewModel.routeGeoCoordinatesLiveData.observe(viewLifecycleOwner, Observer {
            onLoadedMapStopsCoordinates(it)
        })
        viewModel.routePolylineLiveData.observe(viewLifecycleOwner, Observer {
            onLoadedRoutePolyline(it)
        })
    }

    private fun onLoadedMapStopsCoordinates(coordinatePairs: List<Pair<String, GeoCoordinates>>) {
        wayPointsAdapter.submit(coordinatePairs.map { it.first })
        val markerImage = MapImageFactory.fromResource(resources, R.drawable.ic_map_marker)
        coordinatePairs.forEach {
            val markerMetaData = Metadata().apply {
                setString(keyMarkerTitle, it.first)
                setDouble(keyMarkerLatitude, it.second.latitude)
                setDouble(keyMarkerLongitude, it.second.longitude)
            }

            val marker = MapMarker(it.second)
            marker.addImage(markerImage, mapMarkerImageStyle)
            marker.metadata = markerMetaData
            routeStops.add(marker)
            mapView.mapScene.addMapMarker(marker)
        }
    }

    private fun onLoadedRoutePolyline(polyline: GeoPolyline?) {
        if (polyline == null) {
            clearMap()
            return
        }

        routePolyline = MapPolyline(polyline, mapPolylineStyle)
        mapView.mapScene.addMapPolyline(routePolyline!!)
        btnStart.visibility = View.VISIBLE
    }

    private fun clearMap() {
        routePolyline?.let {
            mapView.mapScene.removeMapPolyline(it)
        }
        routeStops.forEach {
            mapView.mapScene.removeMapMarker(it)
        }
        wayPointsAdapter.submit(listOf())
    }

    private fun onLocationChanged(location: GeoCoordinates) {
        currentLocationMarker?.let {
            mapView.mapScene.removeMapMarker(it)
        }

        if (firstLocationUpdate) {
            mapView.camera.target = location
            mapView.camera.zoomLevel = 14.0
            firstLocationUpdate = false
        }

        val mapImage = MapImageFactory.fromResource(resources, R.drawable.ic_marker)
        currentLocationMarker = MapMarker(location)
        currentLocationMarker!!.addImage(mapImage, mapMarkerImageStyle)
        mapView.mapScene.addMapMarker(currentLocationMarker!!)
    }

    private fun onReceivedSuggestions(suggestions: List<Pair<String, GeoCoordinates>>) {
        etStopName.onItemClickListener = AdapterView.OnItemClickListener { parent, _,
                                                                           position, id ->
            hideKeyboard()
            tvNewStopCount.visibility = View.GONE
            etStopName.visibility = View.GONE
            etStopName.text = null

            viewModel.addStop(suggestions[position].first, suggestions[position].second)
        }
        suggestionsAdapter.clear()
        suggestionsAdapter.addAll(suggestions.map { pair ->
            pair.first
        })
        suggestionsAdapter.notifyDataSetChanged()
    }

    private fun initListeners() {
        etStopName.addTextChangedListener {
            val query = it.toString()
            if (query.length < 3) {
                return@addTextChangedListener
            }

            viewModel.searchPlaces(query)
        }
        btnSearch.setOnClickListener {
            btnSearch.hide()
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        btnEnd.setOnClickListener {
            endSimulation()
        }
        btnStart.setOnClickListener {
            it.isSelected = !it.isSelected
            btnEnd.visibility = View.VISIBLE
            if (it.isSelected) {
                btnStart.setText(R.string.pause)
                viewModel.startSimulation()
            } else {
                btnStart.setText(R.string.resume)
                viewModel.pauseSimulation()
            }
        }
    }

    private fun endSimulation() {
        btnEnd.visibility = View.GONE
        btnStart.visibility = View.GONE
        btnStart.setText(R.string.simulate)
        btnStart.isSelected = false
        viewModel.endSimulation()
    }

    private fun initBottomSheetListeners() {
        tvAddStop.setOnClickListener {
            if (tvNewStopCount.isVisible) {
                return@setOnClickListener
            }

            tvNewStopCount.text = getString(R.string.stop_count, wayPointsAdapter.itemCount + 1)
            tvNewStopCount.visibility = View.VISIBLE
            etStopName.visibility = View.VISIBLE
            etStopName.requestFocus()
        }
        btnDrive.setOnClickListener {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            viewModel.buildRoute()
        }
    }

    private fun initBottomSheetBehavior() {
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
        bottomSheetBehavior.isFitToContents = false
        bottomSheetBehavior.expandedOffset =
            (DisplayUtils.getDisplayHeight(requireContext()) * 0.3).toInt()
        bottomSheetBehavior.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    btnSearch.show()
                }
            }
        })
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
    }

    private fun initMap(savedInstanceState: Bundle?) {
        mapView.onCreate(savedInstanceState)
        mapView.mapScene.loadScene(MapStyle.NORMAL_DAY) {
        }
        mapView.gestures.tapListener = TapListener {
            pickMapMarker(it)
        }
        val routeColor = ContextCompat.getColor(requireContext(), R.color.colorRoute).toLong()
        mapPolylineStyle = MapPolylineStyle().apply {
            setColor(routeColor, PixelFormat.ARGB_8888)
            widthInPixels = 8.0
        }
        mapCircleStyle = MapCircleStyle().apply {
            setFillColor(routeColor, PixelFormat.ARGB_8888)
        }
        mapMarkerImageStyle = MapMarkerImageStyle().apply {
            scale = 2f
        }
    }

    private fun pickMapMarker(touchPoint: Point2D) {
        val radiusInPixel = 2f
        mapView.pickMapItems(touchPoint, radiusInPixel,
            PickMapItemsCallback { pickMapItemsResult ->
                if (pickMapItemsResult == null) {
                    return@PickMapItemsCallback
                }

                val topmostMapMarker =
                    pickMapItemsResult.topmostMarker ?: return@PickMapItemsCallback
                showMarkerInfo(topmostMapMarker.metadata)
            })
    }

    private fun showMarkerInfo(markerMetadata: Metadata?) {
        if (markerMetadata == null) {
            return
        }

        val markerInfo = "${markerMetadata.getString(keyMarkerTitle)}: ${markerMetadata.getDouble(
            keyMarkerLatitude
        )}" + ":${markerMetadata.getDouble(keyMarkerLongitude)}"

        Snackbar.make(requireView(), markerInfo, Snackbar.LENGTH_LONG).show()
    }

    private fun permissionGranted(permission: String): Boolean {
        return ActivityCompat.checkSelfPermission(
            requireContext(),
            permission
        ) == PackageManager.PERMISSION_GRANTED
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }
}