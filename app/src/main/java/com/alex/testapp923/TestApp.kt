package com.alex.testapp923

import android.app.Application
import com.alex.testapp923.di.repositoryModule
import com.alex.testapp923.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class TestApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@TestApp)
            modules(repositoryModule, viewModelModule)
        }
    }
}