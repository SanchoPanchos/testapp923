package com.alex.testapp923.di

import com.alex.testapp923.repository.LocationRepository
import com.alex.testapp923.repository.Repository
import com.google.android.gms.location.LocationServices
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val repositoryModule = module {

    single {
        Repository(LocationServices.getFusedLocationProviderClient(androidContext())) as LocationRepository
    }

}