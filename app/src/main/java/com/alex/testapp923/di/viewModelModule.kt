package com.alex.testapp923.di

import com.alex.testapp923.ui.RouteViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel {
        RouteViewModel(get())
    }

}